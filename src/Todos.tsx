import React, { ChangeEvent, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import { getAllTodos } from "./redux/selectors";
import { todosSlice } from "./redux/todosSlice";
import { TodoText } from "./TodoText";

const TodoContainer = styled.div`
  display: flex;
  justify-content: center;
`;

export function Todos() {
  const dispatch = useDispatch();
  const allTodos = useSelector(getAllTodos);

  const handleTodoChange = useCallback(
    (id: number) => (e: ChangeEvent<HTMLInputElement>) => {
      dispatch(todosSlice.actions.checkTodo({ id, checked: e.target.checked }));
    },
    [dispatch]
  );

  const handleDelete = useCallback(
    (id: number) => () => {
      dispatch(todosSlice.actions.deleteTodo(id));
    },
    [dispatch]
  );

  return (
    <>
      {allTodos.map((todo) => (
        <TodoContainer key={todo.id}>
          <input
            type="checkbox"
            checked={todo.checked}
            onChange={handleTodoChange(todo.id)}
          />
          <TodoText todo={todo} />
          <button onClick={handleDelete(todo.id)}>Delete</button>
        </TodoContainer>
      ))}
    </>
  );
}
