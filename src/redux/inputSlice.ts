import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export const inputSlice = createSlice({
  name: "input",
  initialState: "",
  reducers: {
    updateInput(state, action: PayloadAction<string>) {
      return action.payload;
    },
    deleteInput() {
      return "";
    },
  },
});
