import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export type Todo = {
  text: string;
  id: number;
  checked: boolean;
  inEditMode: boolean;
};

let id = 0;

export const todosSlice = createSlice({
  name: "todos",
  initialState: [] as Todo[],
  reducers: {
    addTodo(state, action: PayloadAction<string>) {
      state.push({
        text: action.payload,
        id: id++,
        checked: false,
        inEditMode: false,
      });
    },
    checkTodo(state, action: PayloadAction<{ id: number; checked: boolean }>) {
      for (const todo of state) {
        if (todo.id === action.payload.id) {
          todo.checked = !todo.checked;
        }
      }
    },
    deleteTodo(state, action: PayloadAction<number>) {
      const index = state.findIndex((todo) => todo.id === action.payload);

      state.splice(index, 1);
    },
    toggleEditMode(state, action: PayloadAction<number>) {
      for (const todo of state) {
        if (todo.id === action.payload) {
          todo.inEditMode = !todo.inEditMode;
        }
      }
    },
    updateTodo(state, action: PayloadAction<{ id: number; text: string }>) {
      for (const todo of state) {
        if (todo.id === action.payload.id) {
          todo.text = action.payload.text;
        }
      }
    },
  },
});
