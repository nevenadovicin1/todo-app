import { RootState } from "./rootReducer";

export const getInput = (state: RootState) => state.input;

export const getAllTodos = (state: RootState) => state.todos;
