import { combineReducers } from "redux";
import { todosSlice } from "./todosSlice";
import { inputSlice } from "./inputSlice";

const rootReducer = combineReducers({
  todos: todosSlice.reducer,
  input: inputSlice.reducer,
});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
