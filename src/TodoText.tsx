import React, {
  ChangeEvent,
  KeyboardEvent,
  useCallback,
  useLayoutEffect,
  useRef,
} from "react";
import { useDispatch } from "react-redux";
import { Todo, todosSlice } from "./redux/todosSlice";

type Props = {
  todo: Todo;
};

export function TodoText({ todo }: Props) {
  const inputRef = useRef<HTMLInputElement>(null);
  const dispatch = useDispatch();

  const toggleEditMode = useCallback(() => {
    dispatch(todosSlice.actions.toggleEditMode(todo.id));
  }, [dispatch, todo.id]);

  const handleChange = useCallback(
    (e: ChangeEvent<HTMLInputElement>) => {
      dispatch(
        todosSlice.actions.updateTodo({ id: todo.id, text: e.target.value })
      );
    },
    [dispatch, todo.id]
  );

  const handleKeyDown = useCallback(
    (e: KeyboardEvent<HTMLInputElement>) => {
      if (e.key === "Enter") {
        toggleEditMode();
      }
    },
    [toggleEditMode]
  );

  useLayoutEffect(() => {
    if (todo.inEditMode) {
      inputRef.current!.focus();
    }
  }, [todo.inEditMode]);

  return todo.inEditMode ? (
    <input
      ref={inputRef}
      value={todo.text}
      onBlur={toggleEditMode}
      onChange={handleChange}
      onKeyDown={handleKeyDown}
    />
  ) : (
    <div onClick={toggleEditMode}>{todo.text}</div>
  );
}
