import React from "react";
import styled from "styled-components";

import { Input } from "./Input";
import { Todos } from "./Todos";

const Title = styled.h1`
  width: 100%;
  text-align: center;
`;

export function App() {
  return (
    <>
      <Title>todos</Title>
      <Input />
      <Todos />
    </>
  );
}
