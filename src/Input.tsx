import React, { ChangeEvent, KeyboardEvent, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import { inputSlice } from "./redux/inputSlice";
import { getInput } from "./redux/selectors";
import { todosSlice } from "./redux/todosSlice";

const Container = styled.div`
  display: flex;
  justify-content: center;
  margin-bottom: 20px;
`;

const StyledInput = styled.input`
  width: 500px;
  height: 2em;
  font-size: 2em;
`;

export function Input() {
  const dispatch = useDispatch();

  const handleChange = useCallback(
    (e: ChangeEvent<HTMLInputElement>) => {
      dispatch(inputSlice.actions.updateInput(e.target.value));
    },
    [dispatch]
  );

  const clearInput = useCallback(() => {
    dispatch(inputSlice.actions.deleteInput());
  }, [dispatch]);

  const value = useSelector(getInput);

  const handleKeyDown = useCallback(
    (e: KeyboardEvent<HTMLInputElement>) => {
      if (e.key === "Enter") {
        dispatch(todosSlice.actions.addTodo(value));
        dispatch(inputSlice.actions.deleteInput());
      }
    },
    [dispatch, value]
  );

  return (
    <Container>
      <StyledInput
        placeholder="What needs to be done?"
        onChange={handleChange}
        onKeyDown={handleKeyDown}
        value={value}
      />
      <button onClick={clearInput}>X</button>
    </Container>
  );
}
